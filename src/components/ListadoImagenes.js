import React from 'react';
import Imagen from './Imagen';

const ListadoImagenenes = ({imagenes}) => {
    return ( 
        <div className="col-md-12 p-5 row">
            {imagenes.map(imagen=>(
                <Imagen
                    key={imagen.id}
                    imagen={imagen}
                />
            ))}
        </div>
     );
}
 
export default ListadoImagenenes;