import React,{useState} from 'react';
import close from '../close.svg';

const Imagen = ({imagen}) => {
    const {largeImageURL, likes, previewURL, views} = imagen;

    //state que activará las clases show y showImage
    const [mostrar, actMostrar]= useState(false);

    //acción del onclick
    const elegirImagen=()=>{
        actMostrar(true);
    }
     
    //Función para validar
    let show;
    let showImage;
    const mostrarClase =()=>{
        if(mostrar===true){
            show= 'show';
            showImage='showImage';
            return show && showImage;
        }
    }
    mostrarClase();

    //Funcion para que al hacer click, ocultar la imagen
    const ocultarImagen=()=>{
        actMostrar(false); 
    }

    return ( 
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
            <div className="card">
                <img src={previewURL} alt='tags' className="card-img-top"/>
                <div className=" card-body">
                    <p className="card-text">{likes} Me Gusta</p>
                    <p className="card-text">{views} Vistas</p>
                </div>
                <div className="card-footer">
                        <button
                            onClick={()=>elegirImagen()}
                            rel="noopener noreferrer"
                            className="btn btn-primary btn-block text-white"
                        >Ver Imagen</button>
                </div>
            </div>
            <div className={`imagen-ligth ${show}`} onClick={()=>ocultarImagen()}> 
                <img src={close} alt="icono para cerrar la imagen" className="close"/>
                <img src={largeImageURL} alt="imagen en grande" className={`agregar-img ${showImage}`}/>
            </div> 
        </div>
        
     );
}
 
export default Imagen;